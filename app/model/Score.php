<?php
namespace App\Model;

class Score {
	protected $redis;

	public function __construct(\Predis\Client $redis) {
		$this->redis = $redis;
	}

	/**
	 * storage key by game id
	 *
	 * @param  int  $gameId     id of game
	 * @return string           storage key
	 */
	protected function scoreKey($gameId) {
		return 'game:' . $gameId . ':score';
	}

	/**
	 * save score, overwrite players last record
	 *
	 * @param  int     $gameId     id of game
	 * @param  int     $playerId   id of player
	 * @param  int     $score      score to save
	 * @return int                  affected records
	 */
	public function saveScore($gameId, $playerId, $score) {
		return $this->redis->zadd($this->scoreKey($gameId), $score , $playerId);
	}

	/**
	 * save score, if this player have no higher score yet
	 *
	 * @param  int    $gameId     id of game
	 * @param  int    $playerId   id of player
	 * @param  int    $score      score to save
	 * @return int                affected records
	 */
	public function saveTopScore($gameId, $playerId, $score) {
		$recordedScore = $this->redis->zscore($this->scoreKey($gameId), $playerId);

		if ($recordedScore < $score) {
			return $this->redis->zadd($this->scoreKey($gameId), $score , $playerId);
		} else {
			return 0;
		}
	}

	/**
	 * current rank of player in game
	 *
	 * @param  int    $gameId      id of game
	 * @param  int    $playerId    id of player
	 * @return int                 current rank
	 */
	public function rank($gameId, $playerId) {
		foreach($this->ladder($gameId) AS $record) {
			if ($record['playerId'] == $playerId) {
				return $record['rank'];
			}
		}
	}

	/**
	 * ranked list of scores, from highest to lower
	 *
	 * @param  int    $gameId    id of game
	 * @param  int    $max       limit ladder length, default: -1 (all)
	 * @return array             ['playerId' => int, 'score' => int, 'rank' => int]
	 */
	public function ladder($gameId, $max = -1) {
		$i = 1;
		$lastRank = 1;
		$lastScore = 0;
		$ladder = [];
		foreach ($this->redis->zrevrange($this->scoreKey($gameId), 0, $max, 'WITHSCORES') AS $playerId => $score) {
			$record = [];
			$record['playerId'] = $playerId;
			$record['score'] = $score;

			if ($lastScore == $score) {
				$record['rank'] = $lastRank;
			} else {
				$lastRank = $record['rank'] = $i;
			}

			$lastScore = $score;
			$ladder[] = $record;

			$i++;
		}

		return $ladder;
	}

	/**
	 * validate JSON-RPC request
	 *
	 * @param  array   $request  JSON-RPC request
	 * @return mixed             true on success, array with JSON-RPC error response on failure
	 */
	public static function validateRequest($request) {
		$result = true;

		if (is_array($request)) {
			if (isset($request['id']) && isset($request['method']) && isset($request['params']) && isset($request['jsonrpc'])) {
				if (is_numeric($request['id']) && $request['id'] > 0) {
					if ($request['jsonrpc'] == '2.0') {
						if ($request['method'] == 'save') {
							if (isset($request['params']['playerId']) && is_numeric($request['params']['playerId']) && $request['params']['playerId'] > 0) {
								if (isset($request['params']['score']) && is_numeric($request['params']['score']) && $request['params']['score'] > 0) {
										// everything valid
										return true;
								} else {
									$result = ['code' => -32602, 'message' => 'Score must be number greater than 0', 'data' => $request];
								}
							} else {
								$result = ['code' => -32602, 'message' => 'PlayerId must be number greater than 0', 'data' => $request];
							}
						} else {
							$result = ['code' => -32601, 'message' => 'Invalid method "' . $request['method'] . '"', 'data' => $request];
						}
					} else {
						$result = ['code' => -32603, 'message' => 'Required JSON-RPC version is 2.0', 'data' => $request];
					}
				} else {
					$result = ['code' => -32602, 'message' => 'Id must be number greater than 0', 'data' => $request];
				}

			} else {
				$result = ['code' => -32600, 'message' => 'Invalid request object - missing property', 'data' => $request];
			}
		} else {
			$result = ['code' => -32700, 'message' => 'Unable to parse JSON text', 'data' => $request];
		}

		return $result;
	}
}