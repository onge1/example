<?php

namespace App\Presenters;


class HomepagePresenter extends BasePresenter
{
	public function renderDefault()
	{
		\Predis\Autoloader::register();

		try {
			$redis = new \Predis\Client();

			// This connection is for a remote server
			/*
				$redis = new Predis\Client(array(
				    "scheme" => "tcp",
				    "host" => "153.202.124.2",
				    "port" => 6379
				));
			*/
		}
		catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function actionSaveScore() {
		$this->sendResponse(new JsonResponse(['klic' => 'hodnota']));
	}
}
