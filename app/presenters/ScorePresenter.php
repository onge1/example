<?php
namespace App\Presenters;

class ScorePresenter extends BasePresenter
{
	protected $score;

	public function __construct(\App\Model\Score $score) {
        $this->score = $score;
    }

	public function actionDefault() {
		$request = json_decode(file_get_contents('php://input'), true);

		if (($validationResult = $this->score->validateRequest($request)) === true) {
			$this->score->saveTopScore($request['id'], $request['params']['playerId'], $request['params']['score']);
			$response = $this->score->ladder($request['id']);

			$this->sendResponse(new \Nette\Application\Responses\JsonResponse(['jsonrpc' => '2.0', 'response' => $response, 'id' => $request['id']]));

		} else {
			$this->sendResponse(new \Nette\Application\Responses\JsonResponse(['jsonrpc' => '2.0', 'error' => $validationResult, 'id' => $request['id']]));
		}
	}

	public function actionClient() {

	}
}